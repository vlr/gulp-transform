export function getExtensionFromPath(path: string): string {
  const name = getFullFileName(path);
  const index = name.lastIndexOf(".");
  if (index === -1) { return ""; }
  return name.slice(index);
}

function getFullFileName(path: string): string {
  path = path.replace(/\\/g, "/");
  const slashIndex = path.lastIndexOf("/");
  return path.slice(slashIndex + 1);
}

export function getFileNameFromPath(path: string): string {
  return getFullFileName(path).replace(/\.[^\.]*$/, "");
}
