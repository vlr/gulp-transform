import { SingleFileConverter } from "./file.type";
import { TransformOneStream } from "./transformOneStream";
import { Vinyl, SingleVinylConverter } from "./vinyl.type";
import { readVinyl, toVinyl } from "./convertVinyl";

export function transform(converter: SingleFileConverter): NodeJS.ReadWriteStream {
  return new TransformOneStream(convertFile(converter));
}

function convertFile(converter: SingleFileConverter): SingleVinylConverter {
  return async function (vinyl: Vinyl): Promise<Vinyl> {
    const content = await readVinyl(vinyl);
    const result = await converter(content);
    return toVinyl(result);
  };
}
