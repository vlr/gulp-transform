export interface ContentFile {
  contents: string;
  name: string;
  extension: string;
  cwd: string;
  base: string;
  folder: string;
}

export type MultiFileConverter = (files: ContentFile[]) => ContentFile[] | Promise<ContentFile[]>;

export type SingleFileConverter = (files: ContentFile) => ContentFile | Promise<ContentFile>;
