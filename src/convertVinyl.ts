import * as Vinyl from "vinyl";
import { ContentFile } from "./file.type";
import { getExtensionFromPath, getFileNameFromPath } from "./getFileNameFromPath";

export async function readVinyl(vinyl: Vinyl): Promise<ContentFile> {
  // todo support streams, so far only buffers
  const contents = vinyl.contents.toString();
  return {
    contents,
    name: vinyl.stem || getFileNameFromPath(vinyl.path),
    extension: vinyl.extname || getExtensionFromPath(vinyl.path),
    cwd: vinyl.cwd,
    base: vinyl.base,
    folder: getFolder(vinyl.dirname, vinyl.path)
  };
}

function getFolder(dirname: string, path: string): string {
  if (dirname != null) {
    return dirname.replace(/\\/g, "/");
  }

  path = path.replace(/\\/g, "/");
  const index = path.lastIndexOf("/");
  return path.slice(0, index);
}

export function toVinyl(file: ContentFile): Vinyl {
  if (file == null || file.contents == null) { return null; }
  return new Vinyl({
    cwd: file.cwd,
    base: file.base,
    path: file.folder + "/" + file.name + file.extension,
    contents: Buffer.from(file.contents)
  });
}
