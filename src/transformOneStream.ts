import { Duplex } from "stream";
import { SingleVinylConverter } from "./vinyl.type";

export class TransformOneStream extends Duplex implements NodeJS.ReadWriteStream {
  private piped: NodeJS.WritableStream;

  private ended: boolean;
  private files: number = 0;

  constructor(private convertor: SingleVinylConverter) {
    super();
  }

  // need to implement this, otherwise throws
  public _read(size: number): void {
    this.piped = this.piped;
  }

  public write(file: any): boolean {
    this.files++;
    this.convertor(file)
      .then(res => {
        this.files--;
        if (res != null) {
          this.piped.write(res);
        }
        if (this.files === 0 && this.ended) {
          this.piped.end();
        }
      });
    return true;
  }

  public end(): void {
    if (this.files > 0) {
      this.ended = true;
    } else {
      this.piped.end();
    }
  }

  public pipe<T extends NodeJS.WritableStream>(destination: T, options?: {
    end?: boolean;
  }): T {
    this.piped = destination;
    return super.pipe(destination, options);
  }
}
