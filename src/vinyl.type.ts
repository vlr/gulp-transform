import * as File from "vinyl";

export type Vinyl = File;

export type MultiVinylConverter = (files: Vinyl[]) => Vinyl[] | Promise<Vinyl[]>;

export type SingleVinylConverter = (files: Vinyl) => Vinyl | Promise<Vinyl>;
