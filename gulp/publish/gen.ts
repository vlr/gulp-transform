import * as gulp from "gulp";
import { ContentFile } from "../../src/file.type";
import { transformRange, transform } from "../../src";

export function genMulti(): NodeJS.ReadWriteStream {
  const testDir = "./testDir";
  const filesToExport = <ContentFile[]>[
    {
      contents: "2", name: "file2", extension: ".txt"
    },
    {
      contents: "3", name: "file3", extension: ".txt"
    }
  ];

  return gulp.src(testDir + "/**/*.txt")
    .pipe(transformRange(async (files: ContentFile[]) => {
      const exported = filesToExport.map(f => ({ ...files[0], ...f }));
      return exported;
    }))
    .pipe(gulp.dest(testDir));
}

export function genSingle(): NodeJS.ReadWriteStream {
  const testDir = "./testDir";
  return gulp.src(testDir + "/**/*.txt")
    .pipe(transform(async (file: ContentFile) => {
      return {
        ...file,
        contents: file.contents + "2",
        extension: ".ts"
      };
    }))
    .pipe(gulp.dest(testDir));
}
