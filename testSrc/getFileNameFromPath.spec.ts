import { getFileNameFromPath, getExtensionFromPath } from "../src/getFileNameFromPath";
import { expect } from "chai";

describe("getFileNameFromPath", function (): void {
  it("should return file name without extension", async function (): Promise<void> {
    // arrange
    const src = "c:\\folder.type\\file.type.ts";

    // act
    const result = getFileNameFromPath(src);

    // assert
    expect(result).equals("file.type");
  });

});

describe("getExtensionFromPath", function (): void {
  it("should return file extension with dot", async function (): Promise<void> {
    // arrange
    const src = "c:\\folder.type\\file.type.ts";

    // act
    const result = getExtensionFromPath(src);

    // assert
    expect(result).equals(".ts");
  });
});
