// removing the test, since it is long and unreliable
// import { expect } from "chai";
// import * as fse from "fs-extra";
// import { spawn } from "./spawn";

// describe("transformRange", function (): void {
//   it("should convert one file into 2 files", async function (): Promise<void> {
//     // arrange
//     const testDir = "./testDir";
//     await fse.remove(testDir);
//     await fse.mkdir(testDir);
//     try {
//       await fse.writeFile(testDir + "/file1.txt", "1");

//     // act
//       await spawn("genMulti", "gulp", ["genMulti"]);

//     // assert
//       const folder = await fse.readdir(testDir);
//       expect(folder.length).equals(3);
//       expect(folder.some(file => file.endsWith("file2.txt"))).equals(true);
//       expect(folder.some(file => file.endsWith("file3.txt"))).equals(true);
//       const file2Content = await fse.readFile(testDir + "/file2.txt", "utf8");
//       const file3Content = await fse.readFile(testDir + "/file3.txt", "utf8");
//       expect(file2Content).equals("2");
//       expect(file3Content).equals("3");
//     } finally {
//       await fse.remove(testDir);
//     }
//   });
// });
