import { expect } from "chai";
import { changeFileType } from "../src";


describe("changeFileType", function (): void {

  it("should remove type of file", async function (): Promise<void> {
    // arrange
    const src = "file.type";

    // act
    const result = changeFileType(src, "");

    // assert
    expect(result).equals("file");
  });

  it("should add type of file", async function (): Promise<void> {
    // arrange
    const src = "file";

    // act
    const result = changeFileType(src, "type");

    // assert
    expect(result).equals("file.type");
  });

  it("should change type of file", async function (): Promise<void> {
    // arrange
    const src = "file.type";

    // act
    const result = changeFileType(src, ".state");

    // assert
    expect(result).equals("file.state");
  });

  it("should change type of file", async function (): Promise<void> {
    // arrange
    const src = "file.type";

    // act
    const result = changeFileType(src, ".state");

    // assert
    expect(result).equals("file.state");
  });

  it("should change type of file with path", async function (): Promise<void> {
    // arrange
    const src = "../file.type";

    // act
    const result = changeFileType(src, ".state");

    // assert
    expect(result).equals("../file.state");
  });

  it("should add type to file with path", async function (): Promise<void> {
    // arrange
    const src = "../file";

    // act
    const result = changeFileType(src, ".state");

    // assert
    expect(result).equals("../file.state");
  });

  it("should remove type from file with path", async function (): Promise<void> {
    // arrange
    const src = "../file.type";

    // act
    const result = changeFileType(src, "");

    // assert
    expect(result).equals("../file");
  });
});
