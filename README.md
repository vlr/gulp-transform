# @vlr/gulp-transform
Gulp plugin to transform range of files to another range of files.
Main application is generation of code.

# transformRange

```
import { transformRange, ContentFile } from "@vlr/gulp-transform";

export function generateYourCode(): NodeJS.ReadWriteStream {
	return gulp.src(sourceFiles)
	.pipe(transformRange(myGenerator))
	.pipe(gulp.dest(destinationFolder));
}

function myGenerator(srcFiles: ContentFile[]): ContentFile[] {
  // return new set of files to be written
  return [];
}

```

# transform single

```
import { transform, ContentFile } from "@vlr/gulp-transform";

export function generateYourCode(): NodeJS.ReadWriteStream {
	return gulp.src(sourceFiles)
	.pipe(transform(myGenerator))
	.pipe(gulp.dest(destinationFolder));
}

function myGenerator(file: ContentFile): ContentFile {
  // return new file to be written, change name and/or extension
  return {
    ...file,
    contents: file.contents + "1",
    extension: ".bk"
  };
}

```
